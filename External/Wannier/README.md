Using Wannier90 on the fly in SIESTA calculations
=================================================


Motivations
-----------

Wannier90 is mostly meant to be used as a standalone program, reading
its parameters from input files and writing results to output
files. In these conditions, performing a wannierisation with SIESTA
would involve preparing the input data, putting the SIESTA calculation
on hold, running Wannier90 for each manifold of interest, and then
continuing with the SIESTA calculation. Such a procedure would involve
several manual steps, making it particularly error-prone.

In order to streamline the process we have implemented a simple
mechanism to allow Wannier90 to be called on-the-fly from SIESTA
and to be run several times with different input parameters without
having to stop the driver program. In essence, we invoke wannier90
using a minimal wrapper (see below).

This mechanism can be optionally compiled in SIESTA by setting the
CMake variable

```
   -DWITH_WANNIER90=ON
```

and defining the ``WANNIER90_PACKAGE`` environment variable to point to
a *pristine* wannier90-3.1.0.tar.gz package (possibly remote):

```
    export WANNIER90_PACKAGE=/path/to/wannier90-3.1.0.tar.gz
    export WANNIER90_PACKAGE=https://github.com/wannier-developers/wannier90/archive/v3.1.0.tar.gz
```

The SIESTA build system will unpack and patch the wannier90
distribution automatically, and create the appropriate version
(serial or mpi) of the wrapper.

Simple wrapper for wannier90
----------------------------

Within the context of the Siesta project, a small patch for
wannier90-3.1.0 has been created to enable a simpler interface between
the codes. The main change is the wrapping of the ``wannier_prog``
program to turn it into a subroutine that can be called directly from
another program. That is the only entry point of the API, as
implemented in the ``wannier90_m`` module.

In addition, a simple CMake building system has been implemented to facilitate
the automatic compilation of the wrapper.

The wrapper is maintained in a branch of a forked wannier90 repository in GitHub:

  https://github.com/albgar/wannier90/tree/wrapper-3.1.0

rooted in the 3.1.0 version. Patch files are created by simply doing a `git diff`
from the 3.1.0 version. A copy of the patch file is kept in the `Patches` subdirectoy here.

The ``wannier90_wrapper`` subroutine accepts as arguments:

* The ``seedname`` or file prefix.
* The MPI communicator to be used (only in the MPI version)
* (Optional) nnkp_mode: a flag to request only the generation of a .nnkp file.
* (Optional) dryrun_mode: a flag to perform a simple dry-run to check the input file.
* (Optional) Arrays to hold the information about k-point neighbors
* (Optional) Arrays to hold the information about the unitary matrices

The last two are convenience arguments to enable client codes to extract the needed information
directly, without having to read the .nnkp and .chk files, respectively.

All other interaction with wannier90 is through files:

* ``seedname.win`` (provided by client) is the input file
* ``seedname.amn`` (provided by client) 
* ``seedname.mmn`` (provided by client)
* ``seedname.{eig_ext}`` (provided by client)
* ``seedname.wout`` (generated by wannier90)
*  ... plus other plotting and auxiliary files generated by wannier90.

The standard command-line interaction with wannier90 has been bypassed.

In MPI operation, no initialization is done by wannier90, and the communicator to be used is
passed explicitly. Internally, the communicator is named ``mpi_comm_w90``. The ``mpif.h`` file
is read once in the auxiliary module w90_mpi, which is used by relevant pieces of the code.

Some minor changes have been needed to make sure that all variables are deallocated at the end
of the wannier90 run, so that the wrapper can be called repeatedly without errors.

### Compilation of the wrapper with CMake

(Note that Siesta will do this automatically)

Basic incantation (items in brackets are optional, needed for MPI operation and to help finding
an appropriate Lapack library):

```
  cmake -S. -B _build [ -DWITH_MPI=ON ] [-DLAPACK_LIBRARY=   ] [ -DCMAKE_INSTALL_PREFIX=/path/to/inst ]
  cmake --build _build

  # (optional)  cd _build ; ctest ; cd ..

  # (optional) cmake --install _build
```

  

